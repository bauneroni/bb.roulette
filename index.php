<!DOCTYPE html>
<html lang="en" data-ng-app="bb.roulette">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Belt And Braces - Promenade at a Health Spa</title>
		
		<!-- Vendor stylesheets -->
		<link href="css/vendor/bootstrap.min.css" rel="stylesheet">
		
		<!-- Custom stylesheets -->
		<link href="css/styles.css" rel="stylesheet">
	</head>
<body data-ng-controller="RouletteController">
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" data-ng-if="false">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Project name</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#about">About</a></li>
					<li><a href="#contact">Contact</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="player_container" style="">
				<div class="tv_overlay" style=""></div>
				<div data-video-player="player"></div>
			    <!-- <script>
			      var tag = document.createElement('script');
			      tag.src = "https://www.youtube.com/iframe_api";
			      var firstScriptTag = document.getElementsByTagName('script')[0];
			      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			    </script> -->
			</div>
		</div>
		<div class="row">
			<div class="starter-template">
				<!-- <div class="btn-group">
				  <h5>Video</h5>
				  <button type="button" class="btn btn-default" data-ng-click="ytPlayPause()">PlayPause</button>
				  <button type="button" class="btn btn-default" data-ng-click="ytToggleMute()">ToggleMute</button>
				  <button type="button" class="btn btn-default" data-ng-click="ytPlus30()">+ 30s</button>
				</div> -->
				<div class="btn-group">
				  <h5>Control stuff</h5>
				  <button type="button" class="btn btn-default" data-ng-click="prevTrack()">Prev (A)</button>
				  <button type="button" class="btn btn-default" data-ng-click="playPause()">PlayPause (A+V)</button>
				  <button type="button" class="btn btn-default" data-ng-click="nextTrack()">Next (A)</button>
				  <button type="button" class="btn btn-default" data-ng-click="plus30()">+ 30s (A)</button>
				</div>
			</div>
			<div><b>Playing:</b> {{ isPlaying }}</div>
			<div><b>Current position (CD):</b> {{ trackPosition + 1 }}</div>
			<div><b>Current track:</b> {{ currentTrack.title }}</div>
			<div><b>Current file:</b> {{ currentTrack.file }}</div>
			<div><b>Position in song:</b> {{ player.position }}</div>
			<div><b>Song duration:</b> {{ player.duration }}</div>
			<div><b>Preloaded audio:</b> {{ player.load_percent }} %</div>
		</div>
	</div>
	<!-- /.container -->
	<!-- Vendor scripts -->
	<script src="js/vendor/jquery-1.11.1.min.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/vendor/ie10-viewport-bug-workaround.js"></script>
	<script src="js/vendor/angular.min.js"></script>
	<script src="js/vendor/audio5.js"></script>
	<script src="js/vendor/ngAudio5.js"></script>
	
	<!-- Main app -->
	<script src="js/bb.roulette.js"></script>
	<script src="js/bb.roulette.constants.js"></script>
	<script src="js/bb.roulette.audioplayer.js"></script>
	<script src="js/bb.roulette.videoplayer.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</body>
</html>