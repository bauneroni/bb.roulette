var app = angular.module('bb.roulette.videoplayer', [])

.directive('videoPlayer', ['$window', '$rootScope', function($window, $rootScope) {
	return {
		restrict : 'EA',
		replace : true,
		scope : {
			videoPlayerId : '@videoPlayer'
		},
		template : '<div class="videoPlayer"></div>',
		controller : function($scope, $attrs) {
			$attrs.$set('id', $scope.videoPlayerId);
			
			/*
			 * ## Video player controls
			 */
			$scope.ytPlayPause = function() {
				console.log('ytPlayPause');
			};
			
			$scope.ytPlus30 = function() {
				console.log('ytPlus30');
			}
			
			$scope.ytToggleMute = function() {
				console.log('ytToggleMute');
			}
			
			$window.player = {};
			$window.onYouTubeIframeAPIReady = function() {
				//https://developers.google.com/youtube/player_parameters?hl=de - kthx
		        player = new YT.Player($scope.videoPlayerId, {
		          height: '390',
		          width: '640',
		          videoId: 'hf_QaG8OiJ0',
		          playerVars : {
		        	  autohide : 1,
			          autoplay : 0,
			          controls : 0,
			          disablekb : 1,
			          iv_load_policy : 3,
			          modestbranding : 1,
			          rel: 0,
			          showinfo : 0,
			          start : 20,
			          wmode : 'opaque'
		          },
		          events: {
		            'onReady': $scope.onPlayerReady
		          }
		        });
		      }
			
			$scope.onPlayerReady = function(event) {
				$rootScope.$broadcast('VideoPlayerReady');
//		        event.target.playVideo();
		        event.target.mute();
		      }

			$window.playVideo = function() {
				player.playVideo();
			}
			
			$window.pauseVideo = function() {
				player.pauseVideo();
			}
			
			$window.stopVideo = function() {
				player.stopVideo();
			}
		},
		link : function(scope, element, attrs) {
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/iframe_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		}
	};
}]);