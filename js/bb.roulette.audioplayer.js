var app = angular.module('bb.roulette.audioplayer', ['Audio5'])

.factory('AudioPlayer', ['AudioService', '$q', '$timeout', function(AudioService, $q, $timeout) {
	
	function AudioPlayer(trackPath) {
		this.player = AudioService;
		this.player.load(trackPath);
		this.trackPath = trackPath;
		
		//isReady::Promise
		var isReadyDeferred = $q.defer();
		this.player.on('canplay', function() {
			$timeout(function() {
				isReadyDeferred.resolve('Audio player ready.')
			}, 2000);
		})
		this.isReady = function() {
			return isReadyDeferred.promise;
		};
	}
	
	return (AudioPlayer);
}]);