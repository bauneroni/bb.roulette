var app = angular.module('bb.roulette', ['bb.roulette.constants', 'bb.roulette.audioplayer', 'bb.roulette.videoplayer'])

.controller('RouletteController', ['$scope', '$rootScope', 'AudioPlayer', 'albumData', function($scope, $rootScope, AudioPlayer, albumData) {
	/*
	 * ## Data init
	 */
	$scope.tracks = albumData.tracks || [];
	$scope.trackPosition = 6;
	$scope.currentTrack = $scope.tracks[$scope.trackPosition] || {};
	$scope.trackReady = false;
	$scope.videoReady = false;
	
	/*
	 * ## Player
	 */
	$scope.audioPlayer = new AudioPlayer($scope.currentTrack.file);
	$scope.player = $scope.audioPlayer.player;
	
	/* Heuristic: Wait for video state first then check audio state */
	$scope.$on('VideoPlayerReady', function() {
		console.log('VideoPlayerReady');
		$scope.audioPlayer.isReady().then(function() {
			console.log('AudioPlayerReady');
		});
	});
	
	$scope.player.on('canplay', function() {
		if ($scope.isPlaying) {
			$scope.player.play();
		}
	});
	
	$scope.player.on('ended', function() {
		console.log($scope.currentTrack.file + ' ended');
		$scope.nextTrack();
	});
	
	$scope.player.on('timeupdate', function() {
		$scope.$apply();
    });
	
	$scope.player.on('progress', function() {
		$scope.$apply();
	});
	
	/* 
	 * ## Player controls
	 */
	$scope.isPlaying = false;
	
	$scope.playPause = function() {
		$scope.player.playPause();
		$scope.isPlaying = !$scope.isPlaying;
	};
	
	$scope.nextTrack = function() {
		$scope.player.pause();
		
		$scope.trackPosition = ($scope.trackPosition + 1) % $scope.tracks.length;
		$scope.currentTrack = $scope.tracks[$scope.trackPosition];
		$scope.player.load($scope.currentTrack.file);
		
		if ($scope.isPlaying) {
			$scope.player.play();
		}
	};
	
	$scope.prevTrack = function() {
		$scope.player.pause();
		
		$scope.trackPosition = ($scope.trackPosition - 1) % $scope.tracks.length;
		if ($scope.trackPosition < 0) {
			$scope.trackPosition = $scope.tracks.length + $scope.trackPosition;
		}
		$scope.currentTrack = $scope.tracks[$scope.trackPosition];
		$scope.player.load($scope.currentTrack.file);
		
		if ($scope.isPlaying) {
			$scope.player.play();
		}
	};
	
	/*
	 * ## Debug stuff
	 */
	$scope.plus30 = function() {
		$scope.player.seek(timeStrToSeconds($scope.player.position) + 30);
	};
	
	function timeStrToSeconds(timeStr) {
		var splitTime = timeStr.split(':');
		return parseInt(splitTime[0]) * 60 + parseInt(splitTime[1]); 
	}

}]);