var app = angular.module('bb.roulette.constants', [])

.constant('albumData', {
	artist : 'Belt and Braces',
	title : 'Promenade at a Health Spa',
	tracks : [
	{
		title : 'Permanent Impression',
		file : 'media/01_permanent_impression.mp3'
	},
	{
		title : 'Catch a Glimpse',
		file : 'media/02_catch_a_glimpse.mp3'
	},
	{
		title : 'Escape the Skyscraper',
		file : 'media/03_escape_the_skyscraper.mp3'
	},
	{
		title : 'Driven',
		file : 'media/04_driven.mp3'
	},
	{
		title : 'Thy Kingdom Come',
		file : 'media/05_thy_kingdom_come.mp3'
	},
	{
		title : 'Gasoline Rainbow',
		file : 'media/06_gasoline_rainbow.mp3'
	},
	{
		title : 'One Hundred and Six Miles',
		file : 'media/07_106_miles.mp3'
	},
	{
		title : 'Liquid Memories',
		file : 'media/08_liquid_memories.mp3'
	},
	{
		title : 'The Grinning Cheshire Cat',
		file : 'media/09_the_grinning_cheshire_cat.mp3'
	},
	{
		title : 'A Monument',
		file : 'media/10_a_monument.mp3'
	},
	{
		title : 'Between Horizons',
		file : 'media/11_between_horizons.mp3'
	}]
});